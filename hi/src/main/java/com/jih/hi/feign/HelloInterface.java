package com.jih.hi.feign;

import com.jih.hi.ret.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

/**
 * @Author: jihang
 * @Description: feign
 * @Date: 2019/8/23 17:47
 * @Version: 1.0
 */
@FeignClient(value = "cloud-auth") //value为要负载均衡的spring.application.name
public interface HelloInterface {
    @GetMapping("/users")
    public Response getUserInfo();
}
