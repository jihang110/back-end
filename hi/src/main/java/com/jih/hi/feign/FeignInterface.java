package com.jih.hi.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Author: jihang
 * @Description: feign
 * @Date: 2019/8/23 17:47
 * @Version: 1.0
 */
@FeignClient(value = "cloud-feign") //value为要负载均衡的spring.application.name
public interface FeignInterface {
    @GetMapping("/feign")
    public String getStr();
}
