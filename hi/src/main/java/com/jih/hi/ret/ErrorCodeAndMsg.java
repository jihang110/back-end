package com.jih.hi.ret;


/**
 * Created by Tiger on 2018/10/9.
 */
public enum ErrorCodeAndMsg {

    DUPLICATE_USERNAME("0001", "用户名重复"),
    INSUFFICIENT_STUDENT_NUMBER("0002", "学号长度不足"),
    STUDENT_NUMBER_IS_EMPTY("0003", "学号为空"),
    NETWORK_ERROR("9999", "网络错误，待会重试"),
    ;

    private String code;
    private String msg;

    ErrorCodeAndMsg(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
