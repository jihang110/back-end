package com.jih.hi.controller;

import com.jih.hi.feign.FeignInterface;
import com.jih.hi.feign.HelloInterface;
import com.jih.hi.ret.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * @Author: jihang
 * @Description: ${description}
 * @Date: 2019/8/23 17:55
 * @Version: 1.0
 */
@RestController
public class HelloController {

    @Autowired
    HelloInterface helloInterface;

    @Autowired
    FeignInterface feignInterface;

    @GetMapping("/hello2")
    public Response hello2() {
        return helloInterface.getUserInfo();
    }

    @GetMapping("/feign")
    public String feign() {
        return feignInterface.getStr();
    }
}
