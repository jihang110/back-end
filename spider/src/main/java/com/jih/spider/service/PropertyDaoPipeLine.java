package com.jih.spider.service;


import com.jih.spider.entity.Movie;
import com.jih.spider.entity.MovieContent;
import com.jih.spider.mapper.MovieContentMapper;
import com.jih.spider.mapper.MovieMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;
import us.codecraft.webmagic.utils.FilePersistentBase;

import java.util.List;

@Component
public class PropertyDaoPipeLine extends FilePersistentBase implements Pipeline {
    @Autowired
    private MovieMapper movieMapper;
    @Autowired
    private MovieContentMapper movieContentMapper;

    @Override
    public void process(ResultItems resultItems, Task task) {
        Movie movie = resultItems.get("movie");
        movieMapper.insert(movie);
        List<MovieContent> movieContents = resultItems.get("movieContents");
        movieContentMapper.insertBatchContents(movieContents);
    }
}
