package com.jih.spider.entity;

public class Movie {
    private String id;
    private String name;
    private String year;
    private String rating;
    private String summary;
    private String url;
    private String mainpic;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMainpic() {
        return mainpic;
    }

    public void setMainpic(String mainpic) {
        this.mainpic = mainpic;
    }

    public Movie() {

    }

    public Movie(String id, String mainpic, String name, String year, String rating, String summary, String url) {
        this.id = id;
        this.mainpic = mainpic;
        this.name = name;
        this.year = year;
        this.rating = rating;
        this.summary = summary;
        this.url = url;
    }
}
