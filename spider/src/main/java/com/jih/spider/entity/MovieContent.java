package com.jih.spider.entity;

public class MovieContent {
    private String shortContent;
    private String username;
    private String title;
    private String mid;

    public String getShortContent() {
        return shortContent;
    }

    public void setShortContent(String shortContent) {
        this.shortContent = shortContent;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public MovieContent() {

    }

    public MovieContent(String shortContent, String username, String title, String mid) {
        this.shortContent = shortContent;
        this.username = username;
        this.title = title;
        this.mid = mid;
    }
}
