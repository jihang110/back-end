package com.jih.spider.mapper;

import com.jih.spider.entity.MovieContent;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MovieContentMapper {
    void insertBatchContents(@Param("contents") List<MovieContent> contents);
}
