package com.jih.spider.mapper;

import com.jih.spider.entity.Movie;

public interface MovieMapper {
    void insert(Movie movie);
}
