package com.jih.spider.config;


import com.jih.spider.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.downloader.HttpClientDownloader;
import us.codecraft.webmagic.proxy.Proxy;
import us.codecraft.webmagic.proxy.SimpleProxyProvider;

import javax.annotation.Resource;
import java.util.Random;


/**
 * @author wongH
 * @date 2019/5/7 9:21
 * @Version 1.0
 */
@Component
public class Downloader {

    @Autowired
    private RedisTemplate redisTemplate;


    public HttpClientDownloader newIpDownloader() {
        HttpClientDownloader downloader = new HttpClientDownloader() {
            @Override
            protected void onError(Request request) {
                String[] ips = newIp();
                setProxyProvider(SimpleProxyProvider.from(new Proxy(ips[0], Integer.parseInt(ips[1]))));
            }
        };
        return downloader;
    }

    String[] newIp() {
        Long size = redisTemplate.opsForList().size("ip");
        String ip = redisTemplate.opsForList().index("ip", new Random().nextInt(size.intValue())).toString();
        System.err.println("获取ip===========>" + ip);
        String[] ips = ip.split(":");
        return ips;
    }

}

