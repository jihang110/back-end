package com.jih.spider.controller;

import com.jih.spider.config.Downloader;
import com.jih.spider.entity.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.jih.spider.util.RedisUtil;

import javax.annotation.Resource;
import java.util.List;
import java.util.Random;

/**
 * @Author: jihang
 * @Description: ${description}
 * @Date: 2019/8/21 9:46
 * @Version: 1.0
 */
@RestController
public class TestController {
    private static int ExpireTime = 60;   // redis中存储的过期时间60s

    @Resource
    private RedisUtil redisUtil;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private Downloader downloader;

    @RequestMapping("set")
    public boolean redisset(String key) {
        Movie m = new Movie("test", "test", "test", "test", "test", "test", "test");

        //return redisUtil.set(key,userEntity,ExpireTime);
        return redisUtil.set(key, m);
    }

    @RequestMapping("get")
    public Object redisget(String key) {
        return redisUtil.get(key);
    }

    @RequestMapping("get1")
    public Object rr() {
        return downloader.newIpDownloader();
    }

    @RequestMapping("get2")
    public void rr1() {
        Long size = redisTemplate.opsForList().size("ip");
        List<String> range = redisTemplate.opsForList().range("ip", 0, -1);
        String ip = redisTemplate.opsForList().index("ip", 1).toString();
    }

    @RequestMapping("expire")
    public boolean expire(String key) {
        return redisUtil.expire(key, ExpireTime);
    }
}
