package com.jih.spider.controller;

import com.jih.spider.config.Downloader;
import com.jih.spider.entity.Movie;
import com.jih.spider.mapper.MovieContentMapper;
import com.jih.spider.mapper.MovieMapper;
import com.jih.spider.service.PropertyDaoPipeLine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import us.codecraft.webmagic.Spider;

import java.security.Principal;

@RestController
@RequestMapping("/douban")
public class DoubanSpiderController {
    @Autowired
    private PropertyDaoPipeLine diYiPropertyDaoPipeLine;
    @Autowired
    private Downloader downloader;
//    @Autowired
//    private DouBanRepoPageProcessor douBanRepoPageProcessor;


    @GetMapping(value = "/start")
    public void start() {
        Spider.create(new DouBanRepoPageProcessor())
                .setDownloader(downloader.newIpDownloader())
                //从"https://github.com/code4craft"开始抓
                .addUrl("https://movie.douban.com")
                .addPipeline(diYiPropertyDaoPipeLine)
                //开启5个线程抓取
                .thread(3)
                //启动爬虫
                .setExitWhenComplete(true)
                .runAsync();
    }

}
