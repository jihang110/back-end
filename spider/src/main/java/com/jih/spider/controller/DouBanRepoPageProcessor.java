package com.jih.spider.controller;

import com.jih.spider.config.Agent;
import com.jih.spider.entity.Movie;
import com.jih.spider.entity.MovieContent;
import com.jih.spider.mapper.MovieContentMapper;
import com.jih.spider.mapper.MovieMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: jihang
 * @Description: 豆瓣note爬虫
 * @Date: 2019/8/19 17:38
 * @Version: 1.0
 */
//@Service
public class DouBanRepoPageProcessor implements PageProcessor {
    //    @Autowired
//    private MovieMapper movieMapper;
//    @Autowired
//    private MovieContentMapper movieContentMapper;
    private String agent = Agent.getRandom();

    // 部分一：抓取网站的相关配置，包括编码、抓取间隔、重试次数等
    private Site site = Site.me().
            setUserAgent(agent).
            addCookie("dbcl2", "202385472:IxNQBHkDWHA").
            setRetryTimes(1).
            setSleepTime(3000);

    @Override
    // process是定制爬虫逻辑的核心接口，在这里编写抽取逻辑
    public void process(Page page) {
        // 部分二：定义如何抽取页面信息，并保存下来
        String url = page.getUrl().toString();
        String id = page.getUrl().regex("https://movie\\.douban\\.com/subject/(\\w+)/.*").toString();
        String mainpic = page.getHtml().xpath("//div[@id='mainpic']/a/img/@src").toString();
        String name = page.getHtml().xpath("//span[@property='v:itemreviewed']/text()").toString();
        String year = page.getHtml().xpath("//span[@class='year']/text()").toString();
        String rating = page.getHtml().xpath("//strong[@property='v:average']/text()").toString();
        String summary = page.getHtml().xpath("//span[@property='v:summary']/text()").toString();
        List<String> shortContents = page.getHtml().xpath("//div[@class='short-content']/text()").all();
        List<String> contenters = page.getHtml().xpath("//header[@class='main-hd']/a[@class='name']/text()").all();
        List<String> contentTitle = page.getHtml().xpath("//div[@class='main-bd']/h2/a/text()").all();

        if (id == null) {
            page.setSkip(true);
        } else {
            //插库
            Movie movie = new Movie(id, mainpic, name, year, rating, summary, url);
//            movieMapper.insert(movie);
            page.putField("movie", movie);
            List<MovieContent> movieContents = new ArrayList<>();
            for (int i = 0; i < shortContents.size(); i++) {
                MovieContent movieContent = new MovieContent(shortContents.get(i), contenters.get(i), contentTitle.get(i), id);
                movieContents.add(movieContent);
            }
            page.putField("movieContents", movieContents);
//            movieContentMapper.insertBatchContents(movieContents);
        }

        // 部分三：从页面发现后续的url地址来抓取
//        Selectable links = page.getHtml().links();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        page.addTargetRequests(page.getHtml().links().regex("(https://movie\\.douban\\.com/subject/[1-9]\\d*/*)").all());
    }

    @Override
    public Site getSite() {
        return site;
    }

    //使用代理

    public static void main(String[] args) {
        Spider.create(new DouBanRepoPageProcessor())
                //从"https://github.com/code4craft"开始抓
                .addUrl("https://movie.douban.com")
                //开启5个线程抓取
                .thread(5)
                //启动爬虫
                .run();
    }
}
