/*
 Navicat Premium Data Transfer

 Source Server         : 47.96.5.203
 Source Server Type    : MySQL
 Source Server Version : 50723
 Source Host           : 47.96.5.203:3306
 Source Schema         : spider

 Target Server Type    : MySQL
 Target Server Version : 50723
 File Encoding         : 65001

 Date: 21/08/2019 01:47:42
*/

SET
NAMES
utf8mb4;
SET
FOREIGN_KEY_CHECKS
=
0;

-- ----------------------------
-- Table structure for douban_movie_content
-- ----------------------------
DROP TABLE IF EXISTS `douban_movie_content`;
CREATE TABLE `douban_movie_content`
(
  `id`            int(11) NOT NULL AUTO_INCREMENT COMMENT 'id自增',
  `short_content` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '短评',
  `username`      varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `title`         varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评论标题',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '豆瓣电影短评' ROW_FORMAT = Dynamic;

SET
FOREIGN_KEY_CHECKS
=
1;
