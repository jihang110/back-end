package com.jih.netty.controller;

import com.jih.netty.entity.UserMsg;

/**
 * @Author: jihang
 * @Description: Test
 * @Date: 2019/8/22 15:32
 * @Version: 1.0
 */
public class Test {
    public static void main(String[] args) {
        UserMsg userMsg = new UserMsg();
        userMsg.setId(1111);
        userMsg.setMsg("dsds");
        userMsg.setName("name");
        System.out.println(userMsg.toString());
    }
}
