package com.jih.netty.controller;

import com.jih.netty.feign.HelloInterface;
import com.jih.netty.ret.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: jihang
 * @Description: ${description}
 * @Date: 2019/8/23 17:55
 * @Version: 1.0
 */
@RestController
public class HelloController {

    @Autowired
    HelloInterface helloInterface;

    @GetMapping("/hello2")
    public Response hello2() {
        return helloInterface.getUserInfo();
    }

    @GetMapping("/hello")
    public String hello() {
        return helloInterface.getUserName();
    }

}
