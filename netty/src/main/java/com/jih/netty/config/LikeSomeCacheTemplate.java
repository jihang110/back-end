package com.jih.netty.config;

import com.jih.netty.entity.UserMsg;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @Author: jihang
 * @Description: 聊天内容临时存储
 * @Date: 2019/8/22 15:36
 * @Version: 1.0
 */
@Component
public class LikeSomeCacheTemplate {

    private Set<UserMsg> SomeCache = new LinkedHashSet<>();

    public void save(Object user, Object msg) {
        UserMsg userMsg = new UserMsg();
        userMsg.setName(String.valueOf(user));
        userMsg.setMsg(String.valueOf(msg));
        userMsg.setCreateTime(new Date());
        userMsg.setUpdateTime(new Date());
        SomeCache.add(userMsg);
    }

    public Set<UserMsg> cloneCacheMap() {
        return SomeCache;
    }

    public void clearCacheMap() {
        SomeCache.clear();
    }
}
