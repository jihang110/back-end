package com.jih.netty.config;

import com.jih.netty.entity.UserMsg;
import com.jih.netty.repository.UserMsgRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.concurrent.Future;

/**
 * @Author: jihang
 * @Description: 异步任务处理
 * @Date: 2019/8/22 15:37
 * @Version: 1.0
 */
@Component
public class MsgAsyncTesk {

    @Autowired
    private LikeSomeCacheTemplate cacheTemplate;

    @Autowired
    private UserMsgRepository userMsgRepository;

    @Async
    public Future<Boolean> saveChatMsgTask() throws Exception {
//        System.out.println("启动异步任务");
        Set<UserMsg> set = cacheTemplate.cloneCacheMap();
        for (UserMsg item : set) {
            //保存用户消息
            userMsgRepository.save(item);
        }
        //清空临时缓存
        cacheTemplate.clearCacheMap();
        return new AsyncResult<Boolean>(true);
    }

}
