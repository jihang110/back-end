package com.jih.netty.config;

import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.springframework.stereotype.Component;

/**
 * @Author: jihang
 * @Description: ${description}
 * @Date: 2019/9/4 15:39
 * @Version: 1.0
 */
@Component
public class ChannelsTemplate {
    public static ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    public void save(Channel channel) {
        channels.add(channel);
    }

    public void delete(Channel channel) {
        channels.remove(channel);
    }

    public ChannelGroup getList() {
        return channels;
    }
}
