package com.jih.netty.config;

import feign.RequestInterceptor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: jihang
 * @Description: ${description}
 * @Date: 2019/8/2 15:47
 * @Version: 1.0
 */
@EnableConfigurationProperties
@Configuration
public class OAuth2ClientConfig {
    public final static Log logger = LogFactory.getLog(OAuth2ClientConfig.class);



    /**
     * @Method oauth2FeignRequestInterceptor
     * @Author jihang
     * @Version  1.0
     * @Description feign使用RequestInterceptor拦截器实现request转发
     * @Date 2019/9/3 9:54
     */
    @Bean
    public RequestInterceptor oauth2FeignRequestInterceptor() {
//        return new OAuth2FeignRequestInterceptor(new DefaultOAuth2ClientContext(), clientCredentialsResourceDetails());
        RequestInterceptor requestInterceptor = (requestTemplate) -> {
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            RequestContextHolder.setRequestAttributes(attributes, true);
            HttpServletRequest request = attributes.getRequest();
            Enumeration<String> headerNames = request.getHeaderNames();
            try {
                if (headerNames != null) {
                    while (headerNames.hasMoreElements()) {
                        String name = headerNames.nextElement();
                        String values = request.getHeader(name);
                        requestTemplate.header(name, values);
                    }
                }
                logger.info("接口路径："+request.getRequestURL().toString());
                StringBuffer body = new StringBuffer();
                Enumeration<String> bodyNames = request.getParameterNames();
                if (bodyNames != null) {
                    Map map=new HashMap();
                    while (bodyNames.hasMoreElements()) {
                        String name = bodyNames.nextElement();
                        String values = request.getParameter(name);
                        requestTemplate.query(name, values);
                        map.put(name,values);
                    }
                    logger.info("传入参数："+map);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
        return requestInterceptor;
    }

}
