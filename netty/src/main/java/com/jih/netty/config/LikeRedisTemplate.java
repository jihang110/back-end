package com.jih.netty.config;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author: jihang
 * @Description: 保存用户名称与链接随机ID
 * @Date: 2019/8/22 15:36
 * @Version: 1.0
 */
@Component
public class LikeRedisTemplate {

    private Map<Object, Object> RedisMap = new ConcurrentHashMap<>();

    public void save(Object id, Object name) {
        RedisMap.put(id, name);
    }

    public void delete(Object id) {
        RedisMap.remove(id);
    }

    public Object get(Object id) {
        return RedisMap.get(id);
    }
}
