package com.jih.netty.handler;

import com.jih.netty.config.ChannelsTemplate;
import com.jih.netty.config.LikeRedisTemplate;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.group.ChannelGroup;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.mortbay.util.MultiMap;
import org.mortbay.util.UrlEncoded;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: jihang
 * @Description: url参数处理
 * @Date: 2019/9/4 14:45
 * @Version: 1.0
 */
@ChannelHandler.Sharable
@Component
public class CustomUrlHandler extends ChannelInboundHandlerAdapter {
//    public static ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    @Autowired
    private ChannelsTemplate channelsTemplate;
    @Autowired
    private LikeRedisTemplate redisTemplate;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        // 只针对FullHttpRequest类型的做处理，其它类型的自动放过
        if (msg instanceof FullHttpRequest) {
            FullHttpRequest request = (FullHttpRequest) msg;
            String uri = request.uri();
            int idx = uri.indexOf("?");
            if (idx > 0) {
                String query = uri.substring(idx + 1);
                // uri中参数的解析使用的是jetty-util包，其性能比自定义及正则性能高。
                MultiMap<String> values = new MultiMap<String>();
                UrlEncoded.decodeTo(query, values, "UTF-8");
                String userName = values.get("userName").toString();
                //新用户接入

                System.out.println(ctx.channel().remoteAddress());
                //新用户接入
                Channel incoming = ctx.channel();
//        uName = (String)redisTemplate.get(incoming.id());
                ChannelGroup channels = channelsTemplate.getList();
                for (Channel channel : channels) {
                    channel.writeAndFlush(new TextWebSocketFrame("[新用户] - " + userName + " 加入"));
                }
                redisTemplate.save(incoming.id(), userName);   //存储用户
                channels.add(ctx.channel());
                System.out.println("用户:" + redisTemplate.get(incoming.id()) + "在线");
                request.setUri(uri.substring(0, idx));
            }
        }
        ctx.fireChannelRead(msg);
    }
}
