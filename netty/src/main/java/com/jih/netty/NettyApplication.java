package com.jih.netty;

import com.jih.netty.config.NettyConfig;
import com.jih.netty.server.TCPServer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class NettyApplication implements CommandLineRunner {

    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext context = SpringApplication.run(NettyApplication.class, args);
        //注入NettyConfig 获取对应Bean
        NettyConfig nettyConfig = context.getBean(NettyConfig.class);
        //注入TCPServer 获取对应Bean
        TCPServer tcpServer = context.getBean(TCPServer.class);
        //启动websocket的服务
        tcpServer.start();
    }

    @Override
    public void run(String... args) throws Exception {
        System.err.println("---------------------------------------启动-------------------------------------------");
    }
}
