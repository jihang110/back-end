package com.jih.netty.repository;

import com.jih.netty.entity.UserMsg;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author: jihang
 * @Description: ${description}
 * @Date: 2019/8/22 15:31
 * @Version: 1.0
 */
public interface UserMsgRepository extends JpaRepository<UserMsg, Integer> {
//本次未使用到自定义方法，JPA原生即可
}
