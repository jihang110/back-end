package com.jih.netty.feign;

import com.jih.netty.ret.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Author: jihang
 * @Description: feign
 * @Date: 2019/8/23 17:47
 * @Version: 1.0
 */
@FeignClient(value = "cloud-auth") //value为要负载均衡的spring.application.name
public interface HelloInterface {
    @GetMapping("/users")
    public Response getUserInfo();

    @GetMapping(value = "/users/getName")
    public String getUserName();


}
