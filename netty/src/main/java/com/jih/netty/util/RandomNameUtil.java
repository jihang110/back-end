package com.jih.netty.util;

import java.util.Random;

/**
 * @Author: jihang
 * @Description: ${description}
 * @Date: 2019/8/22 15:27
 * @Version: 1.0
 */
public class RandomNameUtil {

    private static Random ran = new Random();
    private final static int delta = 0x9fa5 - 0x4e00 + 1;

    public static char getName() {
        return (char) (0x4e00 + ran.nextInt(delta));
    }
}
