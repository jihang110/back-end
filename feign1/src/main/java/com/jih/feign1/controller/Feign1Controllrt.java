package com.jih.feign1.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: jihang
 * @Description: ${description}
 * @Date: 2019/9/2 11:11
 * @Version: 1.0
 */
@RestController
@RequestMapping("/feign")
public class Feign1Controllrt {
    @GetMapping
    public String getStr(){
        return "Feign1 currentTimeMillis:"+System.currentTimeMillis();
    }
}
