package com.jih.feign2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: jihang
 * @Description: ${description}
 * @Date: 2019/9/2 11:11
 * @Version: 1.0
 */
@RestController
@RequestMapping("/feign")
public class FeignControllrt {
    @GetMapping
    public String getStr(){
        return "Feign2 currentTimeMillis:"+System.currentTimeMillis();
    }
}
