/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50709
Source Host           : localhost:3306
Source Database       : oauth2

Target Server Type    : MYSQL
Target Server Version : 50709
File Encoding         : 65001

Date: 2019-08-16 18:10:47
*/

SET
FOREIGN_KEY_CHECKS
=
0;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`
(
  `username` varchar(50)  NOT NULL,
  `password` varchar(500) NOT NULL,
  `enabled`  tinyint(1) NOT NULL,
  `email`    varchar(255) DEFAULT NULL,
  `avatar`   varchar(255) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users`
VALUES ('admin', '{bcrypt}$2a$10$SSj5gPQz/6btszPLZcmqFuhXSSeUAEStmL3G2CVVI8NhY9Cb5FTk6', '1', 'jihang110@126.com',
        'https://file.iviewui.com/dist/a0e88e83800f138b94d2414621bd9704.png');
INSERT INTO `users`
VALUES ('user', '{bcrypt}$2a$10$mt/0UEOKYIxWW0m5cyFpaOfFoR.PdTjwtTfDiiDRVJagFj9I4I.Di', '1', null, null);
