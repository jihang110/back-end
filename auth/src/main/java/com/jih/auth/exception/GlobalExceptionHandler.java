package com.jih.auth.exception;

import com.jih.auth.ret.ErrorCodeAndMsg;
import com.jih.auth.ret.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Tiger on 2018/10/9.
 */
@ControllerAdvice
@Slf4j
@ResponseBody
public class GlobalExceptionHandler {

    @ExceptionHandler(CommonException.class)
    public Response handleStudentException(HttpServletRequest request, CommonException ex) {
        Response response;
        log.error("CommonException error code:{},msg:{}", ex.getResponse().getCode(), ex.getResponse().getMsg());
        response = new Response(ex.getResponse().getCode(), ex.getResponse().getMsg());
        return response;
    }

    @ExceptionHandler(Exception.class)
    public Response handleException(HttpServletRequest request, Exception ex) {
        Response response;
        log.error("exception error:{}", ex);
        response = new Response(ErrorCodeAndMsg.NETWORK_ERROR.getCode(),
                ErrorCodeAndMsg.NETWORK_ERROR.getMsg());
        return response;
    }
}
