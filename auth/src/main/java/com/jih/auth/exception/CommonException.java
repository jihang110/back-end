package com.jih.auth.exception;

import com.jih.auth.ret.ErrorCodeAndMsg;

/**
 * @Author: jihang
 * @Description: ${description}
 * @Date: 2019/8/19 15:11
 * @Version: 1.0
 */
public class CommonException extends RuntimeException {
    private static final long serialVersionUID = -6580930884771918032L;

    private final ErrorCodeAndMsg response;

    public CommonException(ErrorCodeAndMsg response) {
        this.response = response;
    }

    public ErrorCodeAndMsg getResponse() {
        return response;
    }

}
