package com.jih.auth.controller;

import com.jih.auth.constants.ParamConstants;
import com.jih.auth.entity.User;
import com.jih.auth.exception.CommonException;
import com.jih.auth.mapper.AuthoritiesMapper;
import com.jih.auth.mapper.UserMapper;
import com.jih.auth.ret.ErrorCodeAndMsg;
import com.jih.auth.ret.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: jihang
 * @Description: ${description}
 * @Date: 2019/8/2 14:53
 * @Version: 1.0
 */
@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private AuthoritiesMapper authoritiesMapper;
    Logger logger = LoggerFactory.getLogger(UserController.class);

    @RequestMapping(value = "/current", method = RequestMethod.GET)
    public Principal getUser(Principal principal) {
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>");
        logger.info(principal.toString());
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>");
        return principal;
    }

    @GetMapping(value = "/getName")
    public String getUserName(Principal principal) {
        return principal.getName();
    }


    @GetMapping
    public Response getUserInfo(Principal principal) {
        String userName = principal.getName();
        User user = userMapper.getUserByName(userName);
        user.refreshAccess();
        return new Response(user);
    }

    @PostMapping
    public void insertUser(@RequestBody User user) {
        // 判断该用户名是否被占用
        if (userMapper.hasUserName(user.getUsername())) {
            throw new CommonException(ErrorCodeAndMsg.DUPLICATE_USERNAME);
        }
        // 插入users表
        userMapper.insertUser(user);

        // 插入authorities表，默认权限为ROLE_USER
        List<String> permissions = new ArrayList<>();
        permissions.add(ParamConstants.ROLE_USER);
        authoritiesMapper.insertBatchPermissions(user.getUsername(), permissions);
    }
}
