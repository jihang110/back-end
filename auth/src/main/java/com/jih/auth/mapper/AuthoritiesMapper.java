package com.jih.auth.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: jihang
 * @Description: ${description}
 * @Date: 2019/8/19 16:18
 * @Version: 1.0
 */
public interface AuthoritiesMapper {
    void insertBatchPermissions(@Param("username") String username, @Param("permissions") List<String> permissions);
}
