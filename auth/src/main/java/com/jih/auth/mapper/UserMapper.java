package com.jih.auth.mapper;

import com.jih.auth.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {
    User getUserByName(String username);

    Boolean hasUserName(String username);

    void insertUser(User user);

    void deleteUser(String username);

    void updateUser(User user);
}
