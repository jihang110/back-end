package com.jih.auth.constants;

/**
 * @Author: jihang
 * @Description: ${description}
 * @Date: 2019/8/19 14:56
 * @Version: 1.0
 */
public class ParamConstants {
    //普通用户权限
    public static final String ROLE_USER = "ROLE_USER";
    //管理员用户权限
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
}
