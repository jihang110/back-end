package com.gupaoedu.vip.spring.demo.action;

import com.gupaoedu.vip.spring.demo.service.IModifyService;
import com.gupaoedu.vip.spring.demo.service.IQueryService;
import com.gupaoedu.vip.spring.formework.annotation.GPAutowired;
import com.gupaoedu.vip.spring.formework.annotation.GPController;
import com.gupaoedu.vip.spring.formework.annotation.GPRequestMapping;
import com.gupaoedu.vip.spring.formework.annotation.GPRequestparam;
import com.gupaoedu.vip.spring.formework.webmvc.GPModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 公布接口URL
 */
@GPController
@GPRequestMapping("/web")
public class MyAction {
    @GPAutowired
    IQueryService queryService;
    @GPAutowired
    IModifyService modifyService;

    @GPRequestMapping("/query.json")
    public GPModelAndView query(HttpServletRequest request, HttpServletResponse response, @GPRequestparam("name") String name){
        String result = queryService.query(name);
        return out(response,result);
    }

    @GPRequestMapping("/add*.json")
    public GPModelAndView add(HttpServletRequest request, HttpServletResponse response,
                              @GPRequestparam("name") String name,@GPRequestparam("addr") String addr){
        String result = modifyService.add(name,addr);
        return out(response,result);
    }

    @GPRequestMapping("/edit.json")
    public GPModelAndView edit(HttpServletRequest request, HttpServletResponse response,
                              @GPRequestparam("id") Integer id,@GPRequestparam("name") String name){
        String result = modifyService.edit(id,name);
        return out(response,result);
    }

    @GPRequestMapping("/remove.json")
    public GPModelAndView remove(HttpServletRequest request, HttpServletResponse response,
                               @GPRequestparam("id") Integer id){
        String result = modifyService.remove(id);
        return out(response,result);
    }

    private GPModelAndView out(HttpServletResponse resp, String str) {
        try {
            resp.getWriter().write(str);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
