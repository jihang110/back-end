package com.gupaoedu.vip.spring.formework.beans;

/**
 * 主要用于封装创建后的对象实例，代理对象（proxy Object）或者原生对象3
 */
public class GPBeanWrapper {
    private Object wappedInstance;
    private Class<?> wrappedClass;
    public GPBeanWrapper(Object wappedInstance){
        this.wappedInstance = wappedInstance;
    }

    public Object getWappedInstance(){
        return this.wappedInstance;
    }

    //返回代理以后的class
    //可能会是这个$Proxy0
    public Class<?> getWrappedClass(){
        return this.wappedInstance.getClass();
    }
}
