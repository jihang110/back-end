package com.gupaoedu.vip.spring.formework.core;

/**
 * 单例工厂的顶层设计
 */
public interface GPBeanFactory {
    Object getBean(String beanName) throws Exception;
    public Object getBean(Class<?> beanClass) throws Exception;
}
