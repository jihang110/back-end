package com.gupaoedu.vip.spring.formework.beans.config;

/**
 * 对象初始化事件设置一种回调机制
 */
public class GPBeanPostProcessor {
    //为在Bean的初始化之前提供回调入口
    public Object postProcessBeforeInitializtion(Object bean,String beanName) throws Exception{
        System.err.println("before调用");
        return bean;
    }
    //为在bean的初始化之后提供回调入口
    public Object postprocessAfterInitialization(Object bean,String beanName) throws Exception{
        System.err.println("after调用");
        return bean;
    }
}
