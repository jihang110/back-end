package com.gupaoedu.vip.spring.formework.context;

import com.gupaoedu.vip.spring.formework.annotation.GPAutowired;
import com.gupaoedu.vip.spring.formework.annotation.GPController;
import com.gupaoedu.vip.spring.formework.annotation.GPService;
import com.gupaoedu.vip.spring.formework.beans.GPBeanWrapper;
import com.gupaoedu.vip.spring.formework.beans.config.GPBeanDefinition;
import com.gupaoedu.vip.spring.formework.beans.config.GPBeanPostProcessor;
import com.gupaoedu.vip.spring.formework.beans.support.GPBeanDefinitionReader;
import com.gupaoedu.vip.spring.formework.beans.support.GPDefaultListableBeanFactory;
import com.gupaoedu.vip.spring.formework.core.GPBeanFactory;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * IOC,DI.MVC,AOP
 *
 * getBean的入口是getBean()方法
 *
 */
public class GPApplicationContext extends GPDefaultListableBeanFactory implements GPBeanFactory{
    private String[] configLocations;
    private GPBeanDefinitionReader reader;

    // 单例的IoC容器缓存，用来保证注册式单例的容器
    private Map<String,Object> factoryBeanObjectCache = new ConcurrentHashMap<String,Object>();
    // 通用的IoC容器缓存，用来存储所有的被代理过的对象
    private Map<String,GPBeanWrapper> factoryBeanInstanceCache = new ConcurrentHashMap<String,GPBeanWrapper>();

    public GPApplicationContext(String... configLocations){
        this.configLocations = configLocations;
        try {
            refresh();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void refresh() throws Exception{
        // 1.定位，定位配置文件
        reader = new GPBeanDefinitionReader(this.configLocations);

        // 2.加载配置文件，扫描相关的类，把他们封装成BeanDefinition
        List<GPBeanDefinition> beanDefinitions = reader.loadBeanDefinitions();

        // 3.注册，把配置信息加到容器里面（伪Ioc容器）
        doRegisterBeanDefinition(beanDefinitions);

        // 4.把不是延时加载的类提前初始化
        doAutoWrited();
    }

    private void doAutoWrited() {
        for (Map.Entry<String, GPBeanDefinition> beanDefinitionEntry:super.beanDefinitionMap.entrySet()) {
            String beanName = beanDefinitionEntry.getKey();
            if(!beanDefinitionEntry.getValue().isLazyInit()){
                try {
                    getBean(beanName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void doRegisterBeanDefinition(List<GPBeanDefinition> beanDefinitions) throws Exception{
        for (GPBeanDefinition beanDefinition:beanDefinitions) {
            if(super.beanDefinitionMap.containsKey(beanDefinition.getFactoryBeanName())){
                throw new Exception("The '"+beanDefinition.getFactoryBeanName()+"' is exists!!");
            }
            super.beanDefinitionMap.put(beanDefinition.getFactoryBeanName(),beanDefinition);
        }
        //到这里为止，容器初始化完毕
    }


    //依赖注入，从这里开始，读取BeanDefiniton中的信息
    //然后通过反射机制创建一个实例并返回
    //Spring的做法是，不会把最原始的对象放出去，会用一个BeanWrapper进行一次包装
    //装饰器模式：
    //1.保留原来的OOP关系
    //2.需要对它进行扩展，增强（为以后的AOP打基础）
    @Override
    public Object getBean(String beanName) throws Exception {
        GPBeanDefinition beanDefinition = super.beanDefinitionMap.get(beanName);

        try {
            //生成通知事件
            GPBeanPostProcessor beanPostProcessor = new GPBeanPostProcessor();

            Object instance = instantiateBean(beanDefinition);

            if (null == instance){return null;}

            //在实例初始化以前调用一次
            beanPostProcessor.postProcessBeforeInitializtion(instance,beanName);

            GPBeanWrapper beanWrapper = new GPBeanWrapper(instance);

            this.factoryBeanInstanceCache.put(beanName,beanWrapper);

            //在初始化以后调用一次
            beanPostProcessor.postprocessAfterInitialization(instance,beanName);

            populateBean(beanName,instance);

            //通过这样调用,相当于给我们自己留了可操作的空间
            return this.factoryBeanInstanceCache.get(beanName).getWappedInstance();
        }catch (Exception e){
//            e.printStackTrace();
            return null;
        }

    }

    @Override
    public Object getBean(Class<?> beanClass) throws Exception {
        return getBean(beanClass.getName());
    }

    public String [] getBeanDefinitionNames(){
        return this.beanDefinitionMap.keySet().toArray(new String[this.beanDefinitionMap.size()]);
    }

    public int getBeandDefintionCount(){
        return this.beanDefinitionMap.size();
    }

    public Properties getConfig(){
        return this.reader.getConfig();
    }

    private void populateBean(String beanName,Object instance){
        Class clazz = instance.getClass();
        if (!(clazz.isAnnotationPresent(GPController.class)||clazz.isAnnotationPresent(GPService.class))){
            return;
        }

        Field [] fields = clazz.getDeclaredFields();

        for (Field field: fields) {
            if (!field.isAnnotationPresent(GPAutowired.class)){continue;}

            GPAutowired autowired = field.getAnnotation(GPAutowired.class);

            String autowiredBeanName = autowired.value().trim();

            if ("".equals(autowiredBeanName)){
                autowiredBeanName = field.getType().getName();
            }

            field.setAccessible(true);

            try {
                //个人理解 把缓存autowire注解注入到instance对应的类中
                //TODO 现在有个问题就是beanName只能是默认的，如果是指定不一样的找不到，因该是在之前的GPBeanWrapper中对他进行适配
                field.set(instance,this.factoryBeanInstanceCache.get(autowiredBeanName).getWappedInstance());
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    //传一个beanDefinition,就返回一个实例bean
    private Object instantiateBean(GPBeanDefinition beanDefinition){
        Object instance = null;
        String className = beanDefinition.getBeanClassName();
        try {
            //因为根据class才能确定一个类是否有实例
            if (this.factoryBeanObjectCache.containsKey(className)){
                instance = this.factoryBeanObjectCache.get(className);
            }else {
                Class<?> clazz = Class.forName(className);
                instance = clazz.newInstance();

                this.factoryBeanObjectCache.put(beanDefinition.getFactoryBeanName(),instance);
            }
            return instance;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
